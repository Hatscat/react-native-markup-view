# @koober/react-native-markup-view

## Projet creation historic

### 1. init the react native module

```sh
yarn global add react-native-cli # required by react-native-module-init, with yarn and pod
npx react-native-module-init
```

```txt
npx : 149 installé(s) en 4.954s
ℹ react-native-module-init 0.3.1
✔ What is the desired native module name? … markupView
✔ Should it be a view? … no / yes
✔ View name is is MarkupView. Continue? … yes
✔ What is the full module package name? … react-native-markup-view
✔ Initial package version is 1.0.0 - continue? … yes
✔ What is the desired native object class name prefix (can be blank)? …
✔ Native class name is MarkupView. Continue? … yes
✔ Which native platforms? › Android, iOS
✔ What is the desired Android package id? … com.koober
✔ Support Apple tvOS (requires react-native-tvos fork)? … no
✔ What is the author name? … Koober Team
✔ What is the author email? … team_tech@koober.co
✔ What is the GitHub user account name? … koober
✔ What license? … MIT
ℹ It is possible to generate an example test app,
ℹ with workarounds in metro.config.js for metro linking issues
ℹ Requirements: react-native-cli and Yarn; pod is needed for iOS
✔ Generate the example app (with workarounds in metro.config.js)? … yes
✔ Example app name? … example
✔ What react-native version to use for the example app (should be at least react-native@0.60)? … react-native@latest
? Show the output of React Native CLI (recommended)? yes
# (...)
ℹ generating App.js in the example app
ℹ rewrite metro.config.js with workaround solutions
✔ example app generated ok
ℹ adding the native library module into the example app as a dependency link
yarn add v1.21.1
[1/4] 🔍  Resolving packages...
[2/4] 🚚  Fetching packages...
[3/4] 🔗  Linking dependencies...
warning "@react-native-community/eslint-config > @typescript-eslint/eslint-plugin > tsutils@3.17.1" has unmet peer dependency "typescript@>=2.8.0 || >= 3.2.0-dev || >= 3.3.0-dev || >= 3.4.0-dev || >= 3.5.0-dev || >= 3.6.0-dev || >= 3.6.0-beta || >= 3.7.0-dev || >= 3.7.0-beta".
[4/4] 🔨  Building fresh packages...
success Saved lockfile.
success Saved 1 new dependency.
info Direct dependencies
└─ react-native-markup-view@0.0.0
info All dependencies
└─ react-native-markup-view@0.0.0
✨  Done in 3.06s.
✔ added the native library module into the example app as a dependency link - ok
ℹ checking that the pod tool can show its version
✔ pod tool ok
ℹ starting additional pod install in ios subdirectory of example app
Detected React Native module pod for react-native-markup-view
Analyzing dependencies
Downloading dependencies
Installing react-native-markup-view (1.0.0)
Generating Pods project
Integrating client project
Pod installation complete! There are 34 dependencies from the Podfile and 38 total pods installed.
✔ additional pod install ok
💡 check out the example app in react-native-markup-view/example
ℹ (/Users/lucienboudy/Workspace/react-native-markup-view/example)
💡 recommended: run Metro Bundler in a new shell
ℹ (cd react-native-markup-view/example && yarn start)
💡 enter the following commands to run the example app:
ℹ cd react-native-markup-view/example
ℹ react-native run-android
ℹ react-native run-ios
⚠ first steps in case of a clean checkout
ℹ run Yarn in react-native-markup-view/example
ℹ (cd react-native-markup-view/example && yarn)
ℹ do `pod install` for iOS in react-native-markup-view/example/ios
ℹ (cd react-native-markup-view/example/ios && pod install)
```

### 2. init git

```sh
cd react-native-markup-view
git init
# then improve gitignore with http://gitignore.io/api/reactnative
```

### 3. setup development environnement

* .editorconfig

* vscode/
  * settings.json
  * extensions.json

* example/.eslintrc.json

### bad result... try again with another tool

```sh
mv react-native-markup-view react-native-markup-view-old
```

### 1 bis

```txt
npx @react-native-community/bob create react-native-markup-view
? What is the name of the npm package? react-native-markup-view
? What is the description for the package? A react-native view that can display selectable text from a markup language like HTML.
? What is the name of package author? Koober Team
? What is the email address for the package author? team_tech@koober.com
? What is the URL for the package author? https://gitlab.com/koober-sas
? What is the URL for the repository? https://gitlab.com/koober-sas/react-native-markup-view
Project created successfully at react-native-markup-view!

Get started with the project:

  $ yarn bootstrap

Run the example app on iOS:

  $ yarn example ios

Run the example app on Android:

  $ yarn example android

Good luck!
```

```sh
cd react-native-markup-view
yarn bootstrap
git init
```

### 2 bis. init git

```sh
git init
```

### 3 bis. setup dev env

> project already have typescript, eslint, prettier, jest, husky, editor config

* vscode/
  * settings.json
  * extensions.json

MarkupView

## Useful resources

### RN lib

* <https://github.com/react-native-community/bob>
* <https://reactnative.dev/docs/native-components-android>
* <https://reactnative.dev/docs/native-components-ios>

### Languages

* <https://learnxinyminutes.com/docs/objective-c/>
* <https://learnxinyminutes.com/docs/swift/>
* <https://learnxinyminutes.com/docs/kotlin/>

### Kotlin multiplatform

* <https://play.kotlinlang.org/hands-on/Targeting%20iOS%20and%20Android%20with%20Kotlin%20Multiplatform/01_Introduction>
* <https://github.com/guardian/react-native-with-kotlin>
* <https://kotlinlang.org/docs/reference/mpp-dsl-reference.html>
* <https://docs.gradle.org/current/userguide/gradle_wrapper.html#gradle_wrapper>

### Swift

* <https://teabreak.e-spres-oh.com/swift-in-react-native-the-ultimate-guide-part-2-ui-components-907767123d9e>
* <https://github.com/react-native-kit/react-native-track-player/>
* <https://medium.com/ios-os-x-development/swift-and-objective-c-interoperability-2add8e6d6887>
* <https://medium.com/@jjdanek/react-native-calling-class-methods-on-native-swift-views-521faf44f3dc>
* <https://medium.com/@ttqluong93/counterapp-counterview-setcount-unrecognized-selector-sent-to-instance-d6b778078743>

### Kotlin (android)

* <https://callstack.com/blog/writing-a-native-module-for-react-native-using-kotlin/>
