import { parseHTML, HTMLDocument } from './parser';
import { getDifferences, combineNodes, VirtualDOM, getNodePredictableId } from './virtual-dom';

describe('combineNodes', () => {
  const fromHTML = (html: string): HTMLDocument => {
    const parsed = parseHTML(html);
    if (parsed.ok) {
      return parsed.result;
    }
    throw new Error('Malformed HTML error');
  };
  test('should encode html entities', () => {
    const result = fromHTML('foo&nbsp;bar');
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const id = getNodePredictableId(result[0]!, 0);
    expect(combineNodes(result)).toEqual({
      [id]: expect.objectContaining({
        data: 'foo&nbsp;bar',
      }),
    });
  });
});

describe('getDifferences()', () => {
  const fromHTML = (html: string): VirtualDOM => {
    const parsed = parseHTML(html);
    if (parsed.ok) {
      return combineNodes(parsed.result);
    }
    throw new Error('Malformed HTML error');
  };
  test('should delete everything', () => {
    expect(getDifferences(fromHTML('<h1>TEST</h1>'), {})).toEqual([{ op: 'delete_everything' }]);
  });

  test('should be no diff', () => {
    const astBefore = fromHTML('<p>this is a test</p>');
    const astAfter = fromHTML('<p>this is a test</p>');
    expect(getDifferences(astBefore, astAfter)).toEqual([]);
  });

  test('should insert nodes', () => {
    const astBefore = {};
    const astAfter = fromHTML('<p>foo</p><p>bar</p>foobar');
    expect(getDifferences(astBefore, astAfter)).toEqual([
      { op: 'delete_everything' },
      {
        op: 'insert',
        id: expect.stringMatching('p#0'),
        node: {
          name: 'p',
          childIndex: 0,
          attribs: {},
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('p#1'),
        node: {
          name: 'p',
          childIndex: 1,
          attribs: {},
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('foobar'),
        node: {
          data: 'foobar',
          childIndex: 2,
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('foo#0'),
        node: {
          childIndex: 0,
          data: 'foo',
          parentId: expect.stringMatching('p#0'),
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('bar#0'),
        node: {
          data: 'bar',
          childIndex: 0,
          parentId: expect.stringMatching('p#1'),
        },
      },
    ]);
  });

  test('should return correct diffs', () => {
    const astBefore = fromHTML('<p>this is a test</p>');
    const astAfter = fromHTML('<p>this <mark>is</mark> a test</p>');
    expect(getDifferences(astBefore, astAfter)).toEqual([
      {
        op: 'delete',
        id: expect.stringMatching(encodeURI('this is a test')),
      },
      {
        op: 'insert',
        id: expect.stringMatching(encodeURI('this ')),
        node: {
          data: encodeURI('this '),
          parentId: expect.stringMatching('p'),
          childIndex: 0,
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('mark'),
        node: {
          name: 'mark',
          parentId: expect.stringMatching('p'),
          childIndex: 1,
          attribs: {},
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching(encodeURI(' a test')),
        node: {
          data: encodeURI(' a test'),
          parentId: expect.stringMatching('p'),
          childIndex: 2,
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('is'),
        node: {
          data: 'is',
          parentId: expect.stringMatching('mark'),
          childIndex: 0,
        },
      },
    ]);
  });

  test('should return update for attribs changes', () => {
    const astBefore = fromHTML('<p>this is a test</p>');
    const astAfter = fromHTML('<p class="test">this is a test</p>');
    expect(getDifferences(astBefore, astAfter)).toEqual([
      {
        op: 'update',
        id: expect.stringMatching('p'),
        node: {
          name: 'p',
          childIndex: 0,
          attribs: {
            class: 'test',
          },
        },
      },
    ]);
  });

  test('should insert node at the right position', () => {
    const astBefore = fromHTML('<div><p>foo</p><p>bar</p></div>');
    const astAfter = fromHTML('<div><p>foo</p>foobar<p>bar</p></div>');
    expect(getDifferences(astBefore, astAfter)).toEqual([
      {
        op: 'delete',
        id: expect.stringMatching('p#1'),
      },
      {
        op: 'delete',
        id: expect.stringMatching('bar'),
      },
      {
        op: 'insert',
        id: expect.stringMatching('foobar'),
        node: {
          data: 'foobar',
          parentId: expect.stringMatching('div'),
          childIndex: 1,
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('p#2'),
        node: {
          name: 'p',
          parentId: expect.stringMatching('div'),
          childIndex: 2,
          attribs: {},
        },
      },
      {
        op: 'insert',
        id: expect.stringMatching('bar'),
        node: {
          data: 'bar',
          parentId: expect.stringMatching('p#2'),
          childIndex: 0,
        },
      },
    ]);
  });
});
