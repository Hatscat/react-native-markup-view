import type { ConsoleAction } from './action';

export default function log(level: ConsoleAction['level'], ...args: unknown[]): void {
  if (__DEV__) {
    // eslint-disable-next-line no-console
    console[level]('[MarkupView]', ...args);
  }
}
