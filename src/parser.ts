import { Parser as HTMLParser } from 'htmlparser2';
import type { Handler } from 'htmlparser2/lib/Parser';
import { decodeHTMLEntities } from './htmlEntities';

interface HTMLBaseNode<Type extends string> {
  type: Type;
}

interface HTMLDataNode<Type extends string> extends HTMLBaseNode<Type> {
  data: string;
  textStartPosition: number;
  textEndPosition: number;
}

type Properties<P> = Partial<Omit<P, 'type'>>;

export type HTMLTextNode = HTMLDataNode<'text'>;

export function HTMLTextNode(properties?: Properties<HTMLTextNode>): HTMLTextNode {
  return {
    type: HTMLTextNode.type,
    data: '',
    textStartPosition: 0,
    textEndPosition: 0,
    ...properties,
  };
}
HTMLTextNode.type = 'text' as const;

export interface HTMLTagNode extends HTMLBaseNode<'tag'> {
  name: string;
  attribs: Readonly<{ [name: string]: string | Record<string, unknown> | undefined }>;
  children: ReadonlyArray<HTMLNode>;
}

export function HTMLTagNode(properties?: Properties<HTMLTagNode>): HTMLTagNode {
  return {
    type: HTMLTagNode.type,
    name: 'div',
    attribs: {},
    children: [],
    ...properties,
  };
}
HTMLTagNode.type = 'tag' as const;

export type HTMLNode = HTMLTextNode | HTMLTagNode;

export const HTMLNode = Object.freeze({
  isTextNode: (node: HTMLNode): node is HTMLTextNode => node.type === HTMLTextNode.type,
  isTagNode: (node: HTMLNode): node is HTMLTagNode => node.type === HTMLTagNode.type,
  createTextNode: HTMLTextNode,
  createTagNode: HTMLTagNode,
});

export type HTMLDocument = ReadonlyArray<HTMLNode>;

export const HTMLDocument = Object.freeze({
  empty: Object.freeze([]) as HTMLDocument,
  flatMap(tree: HTMLDocument, fn: (node: HTMLTextNode) => HTMLDocument): ReadonlyArray<HTMLNode> {
    return tree.reduce<ReadonlyArray<HTMLNode>>((returnValue, node) => {
      switch (node.type) {
        case HTMLTextNode.type:
          return returnValue.concat(fn(node));
        case HTMLTagNode.type:
          return returnValue.concat(
            node.children.length > 0
              ? {
                  ...node,
                  children: HTMLDocument.flatMap(node.children, fn),
                }
              : node
          );
        default:
          return returnValue;
      }
    }, []);
  },
});

export type ParserResult = { ok: true; result: HTMLDocument } | { ok: false; error: unknown };

export type Parser = (content: string, options?: ParserOptions) => ParserResult;

export type ParserOptions = {
  /***
   * Indicates whether the whitespace in text nodes should be normalized
   * (= all whitespace should be replaced with single spaces). The default value is "false".
   */
  normalizeWhitespace?: boolean;
};

export const parseHTML: Parser = (html, options) => {
  const domHandler = createDOMHandler({
    normalizeWhitespace: options?.normalizeWhitespace ?? true,
  });
  const parser = new HTMLParser(domHandler, {
    decodeEntities: false,
    xmlMode: false,
  });
  parser.write(html);
  parser.done();

  return domHandler.getResult();
};

export const parsePlainText: Parser = (utf8RawString, options) => ({
  ok: true,
  result: [
    {
      type: 'text',
      data: utf8RawString,
      textStartPosition: 0,
      textEndPosition: utf8RawString.length,
    },
  ],
});

type DOMHandlerOptions = {
  /***
   * Indicates whether the whitespace in text nodes should be normalized
   * (= all whitespace should be replaced with single spaces). The default value is "false".
   */
  normalizeWhitespace?: boolean;
};

type DOMHandler = { getResult: () => ParserResult } & Pick<
  Handler,
  | 'onparserinit'
  | 'onreset'
  | 'onend'
  | 'onerror'
  | 'onclosetag'
  | 'onopentag'
  | 'ontext'
  | 'oncdatastart'
  | 'oncdataend'
  | 'onprocessinginstruction'
  | 'oncomment'
>;

function createDOMHandler(options: DOMHandlerOptions = {}): DOMHandler {
  const reWhitespace = /\s+/g;
  interface ParserInterface {
    startIndex: number | null;
    endIndex: number | null;
  }

  type DOMHandlerState = {
    /** The constructed DOM */
    dom: HTMLNode[];
    /** Indicate if the result is ok */
    ok: boolean;
    /** Indicated whether parsing has been completed. */
    done: boolean;
    /** Reference to the parser instance. Used for location information. */
    parser: ParserInterface | undefined;
    lastError: Error | undefined;
    /** Stack of open tags. */
    tagStack: HTMLTagNode[];
    /** Latest text position */
    currentTextPosition: number;
  };

  const state: DOMHandlerState = {
    dom: [],
    ok: true,
    done: false,
    tagStack: [],
    lastError: undefined,
    parser: undefined,
    currentTextPosition: 0,
  };
  const setState = (newState: Partial<DOMHandlerState>): void => {
    Object.assign(state, newState);
  };

  function getResult(): ParserResult {
    return state.ok
      ? {
          ok: state.ok,
          result: state.dom,
        }
      : {
          ok: state.ok,
          error: state.lastError,
        };
  }

  function onparserinit(parser: ParserInterface): void {
    setState({
      parser,
    });
  }

  // Resets the handler back to starting state
  function onreset(): void {
    setState({
      dom: [],
      done: false,
      tagStack: [],
      parser: state.parser ?? undefined,
    });
  }

  // Signals the handler that parsing is done
  function onend(): void {
    if (!state.done) {
      setState({
        done: true,
        parser: undefined,
      });
    }
  }

  function onerror(error: Error): void {
    setState({
      ok: false,
      lastError: error,
    });
  }

  function onclosetag(): void {
    setState({
      tagStack: state.tagStack.slice(0, -1),
    });
  }

  function onopentag(name: string, attribs: HTMLTagNode['attribs']): void {
    const node = HTMLNode.createTagNode({
      name,
      attribs,
    });
    addNode(node);
    setState({
      tagStack: state.tagStack.concat(node),
    });
  }

  function ontext(data: string): void {
    const nodeData = decodeHTMLEntities(options.normalizeWhitespace === true ? data.replace(reWhitespace, ' ') : data);
    const textEndPosition = state.currentTextPosition + nodeData.length;

    const node = HTMLNode.createTextNode({
      data: nodeData,
      textStartPosition: state.currentTextPosition,
      textEndPosition,
    });

    addNode(node);

    setState({
      currentTextPosition: textEndPosition,
    });
  }

  function oncdatastart(): void {}

  function oncdataend(): void {}

  function oncomment(data: string): void {}

  function onprocessinginstruction(name: string, data: string): void {
    // const node = new ProcessingInstruction(name, data);
    // addNode(node);
  }

  function addNode(node: HTMLNode): void {
    const parent = state.tagStack[state.tagStack.length - 1];
    const siblings = parent != null ? parent.children : state.dom;
    // Force mutability as private state
    (siblings as Array<HTMLNode>).push(node);
  }

  return {
    onparserinit,
    onreset,
    onend,
    onerror,
    onclosetag,
    onopentag,
    ontext,
    oncdatastart,
    oncdataend,
    onprocessinginstruction,
    oncomment,
    getResult,
  };
}
