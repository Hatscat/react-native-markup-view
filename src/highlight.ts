import { HTMLDocument, HTMLNode, HTMLTextNode } from './parser';
import { Highlight } from './native';

export function combineHighlights(document: HTMLDocument, highlights: ReadonlyArray<Highlight>): HTMLDocument {
  return highlights.length === 0
    ? document
    : // flatten AST from lib and map text node
      HTMLDocument.flatMap(document, (node) =>
        // for all highlights check if text node is a part of it
        highlights.reduce<ReadonlyArray<HTMLNode>>(
          // At 1st => allHighlightNodes = [node] = array of one text node before applying highlight on it
          (allHighlightNodes, highlight) => {
            // Highlights are added to the 1st (big) text node so we split it
            // example:
            // 1 : allHighlightNodes = ['toto tata tutu'] => ['toto', mark : { text: 'tata'}, 'tutu']
            // 2 : allHighlightNodes = ['toto', mark : { text: 'tata'}, 'tutu'] => ['toto', mark : { text: 'tata'}, 't', mark : { text: 'ut'}, 'u']
            return allHighlightNodes.reduce<ReadonlyArray<HTMLNode>>(
              (ongoingHighlightNodes: ReadonlyArray<HTMLNode>, htmlNode: HTMLNode): ReadonlyArray<HTMLNode> => {
                // we can have only type 'text' and mark 'tag'
                if (htmlNode.type === 'text') {
                  const firstNodes = ongoingHighlightNodes.slice(0, ongoingHighlightNodes.indexOf(htmlNode));
                  const newNodes = handleHighlight(htmlNode, highlight);
                  const endNodes = ongoingHighlightNodes.slice(ongoingHighlightNodes.indexOf(htmlNode) + 1);

                  return firstNodes.concat(newNodes, endNodes);
                }

                return ongoingHighlightNodes;
              },
              allHighlightNodes
            );
          },
          [node]
        )
      );
}

function handleHighlight(node: HTMLTextNode, highlight: Highlight): ReadonlyArray<HTMLNode> {
  return intersectHighlight(node, highlight) ? mergeWithHighlight(node, highlight) : [node];
}

export function intersectHighlight(node: HTMLTextNode, highlight: Highlight): boolean {
  const { start, end } = highlight;
  const { textStartPosition: nodeStart, textEndPosition: nodeEnd } = node;

  const isInsideRange = start <= nodeStart && end >= nodeEnd;
  const isAtRangeStart = start > nodeStart && start < nodeEnd;
  const isAtRangeEnd = end > nodeStart && end < nodeEnd;

  return isInsideRange || isAtRangeStart || isAtRangeEnd;
}

export function mergeWithHighlight(node: HTMLTextNode, highlight: Highlight): ReadonlyArray<HTMLNode> {
  const { start, end } = highlight;
  const { textStartPosition: nodeStart, textEndPosition: nodeEnd } = node;

  const markStart = Math.max(nodeStart, start);
  const markEnd = Math.min(nodeEnd, end);

  const nodeBefore =
    nodeStart < markStart
      ? HTMLNode.createTextNode({
          data: node.data.slice(0, markStart - nodeStart),
          textStartPosition: nodeStart,
          textEndPosition: markStart,
        })
      : null;

  const nodeAfter =
    nodeEnd > markEnd
      ? HTMLNode.createTextNode({
          data: node.data.slice(markEnd - nodeStart),
          textStartPosition: markEnd,
          textEndPosition: nodeEnd,
        })
      : null;

  const markChild =
    nodeStart === markStart && nodeEnd === markEnd
      ? node
      : HTMLNode.createTextNode({
          data: node.data.slice(markStart - nodeStart, markEnd - nodeStart),
          textStartPosition: markStart,
          textEndPosition: markEnd,
        });

  const markNode = HTMLNode.createTagNode({
    name: 'mark',
    children: [markChild],
    attribs: {
      ...(highlight.id != null
        ? {
            'data-highlight-id': String(highlight.id),
          }
        : undefined),
    },
  });

  return [nodeBefore, markNode, nodeAfter].filter((currentNode) => currentNode != null) as ReadonlyArray<HTMLNode>;
}
