import { parseHTML, HTMLDocument, HTMLNode } from './parser';
import { combineHighlights, intersectHighlight, mergeWithHighlight } from './highlight';
import { Highlight } from './native';

const fromHTML = (html: string): HTMLDocument => {
  const parsed = parseHTML(html);
  if (!parsed.ok) {
    throw new Error('Malformed HTML error');
  }

  return parsed.result;
};

describe('combineHighlights()', () => {
  test('should return the same AST provided if no highlights', () => {
    const parsed = fromHTML('<h1>TEST</h1>');
    expect(combineHighlights(parsed, [])).toEqual(parsed);
  });

  test('should return AST with one mark tag', () => {
    expect(
      combineHighlights(fromHTML('<h1>TEST</h1><h2>toto</h2>'), [
        { start: 0, end: 1, text: 'T', id: 'highlight_id' },
        { start: 2, end: 3, text: 'S', id: 'highlight_id2' },
        { start: 4, end: 7, text: 'tot', id: 'highlight_id3' },
      ])
    ).toEqual([
      HTMLNode.createTagNode({
        name: 'h1',
        attribs: {},
        children: [
          HTMLNode.createTagNode({
            name: 'mark',
            attribs: {
              'data-highlight-id': 'highlight_id',
            },
            children: [
              HTMLNode.createTextNode({
                data: 'T',
                textStartPosition: 0,
                textEndPosition: 1,
              }),
            ],
          }),
          HTMLNode.createTextNode({
            data: 'E',
            textStartPosition: 1,
            textEndPosition: 2,
          }),
          HTMLNode.createTagNode({
            name: 'mark',
            attribs: {
              'data-highlight-id': 'highlight_id2',
            },
            children: [
              HTMLNode.createTextNode({
                data: 'S',
                textStartPosition: 2,
                textEndPosition: 3,
              }),
            ],
          }),
          HTMLNode.createTextNode({
            data: 'T',
            textStartPosition: 3,
            textEndPosition: 4,
          }),
        ],
      }),
      HTMLNode.createTagNode({
        name: 'h2',
        attribs: {},
        children: [
          HTMLNode.createTagNode({
            name: 'mark',
            attribs: {
              'data-highlight-id': 'highlight_id3',
            },
            children: [
              HTMLNode.createTextNode({
                data: 'tot',
                textStartPosition: 4,
                textEndPosition: 7,
              }),
            ],
          }),
          HTMLNode.createTextNode({
            data: 'o',
            textStartPosition: 7,
            textEndPosition: 8,
          }),
        ],
      }),
    ]);
  });
});

describe('intersectHighlight()', () => {
  test('should return false when no intersection', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight = Highlight.create({ start: 77, end: 98 });
    expect(intersectHighlight(textNode, highlight)).toBe(false);
  });

  test('should return true when node is inside highlight', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight = Highlight.create({ start: 1, end: 98 });
    expect(intersectHighlight(textNode, highlight)).toBe(true);
  });

  test('should return true when highlight is inside the node', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight = Highlight.create({ start: 4, end: 6 });
    expect(intersectHighlight(textNode, highlight)).toBe(true);
  });

  test('should return true when highlight fit the node position', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight = Highlight.create({ start: 3, end: 7 });
    expect(intersectHighlight(textNode, highlight)).toBe(true);
  });

  test('should return true when one highlight end exceeds the node', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight1 = Highlight.create({ start: 3, end: 98 });
    const highlight2 = Highlight.create({ start: 1, end: 7 });
    expect(intersectHighlight(textNode, highlight1)).toBe(true);
    expect(intersectHighlight(textNode, highlight2)).toBe(true);
  });

  test('should return true when highlight is partially in the node', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight1 = Highlight.create({ start: 5, end: 98 });
    const highlight2 = Highlight.create({ start: 1, end: 5 });
    const highlight3 = Highlight.create({ start: 3, end: 5 });
    const highlight4 = Highlight.create({ start: 4, end: 7 });
    const highlight5 = Highlight.create({ start: 6, end: 7 });
    expect(intersectHighlight(textNode, highlight1)).toBe(true);
    expect(intersectHighlight(textNode, highlight2)).toBe(true);
    expect(intersectHighlight(textNode, highlight3)).toBe(true);
    expect(intersectHighlight(textNode, highlight4)).toBe(true);
    expect(intersectHighlight(textNode, highlight5)).toBe(true);
  });
});

describe('mergeWithHighlight()', () => {
  test('should return one markNode with one textnode child when node is inside highlight', () => {
    const textNode = HTMLNode.createTextNode({
      data: 'hey!',
      textStartPosition: 3,
      textEndPosition: 7,
    });
    const highlight = Highlight.create({ start: 1, end: 98 });
    expect(mergeWithHighlight(textNode, highlight)).toEqual([
      HTMLNode.createTagNode({
        name: 'mark',
        children: [textNode],
      }),
    ]);
  });

  test('should return 3 nodes highlight is inside node', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight = Highlight.create({ start: 4, end: 6 });
    expect(mergeWithHighlight(textNode, highlight)).toEqual([
      HTMLNode.createTextNode({
        data: 'h',
        textStartPosition: 3,
        textEndPosition: 4,
      }),
      HTMLNode.createTagNode({
        name: 'mark',
        children: [HTMLNode.createTextNode({ data: 'ey', textStartPosition: 4, textEndPosition: 6 })],
      }),
      HTMLNode.createTextNode({ data: '!', textStartPosition: 6, textEndPosition: 7 }),
    ]);
  });

  test('should return 2 nodes when highlight split the node', () => {
    const textNode = HTMLNode.createTextNode({ data: 'hey!', textStartPosition: 3, textEndPosition: 7 });
    const highlight1 = Highlight.create({ start: 1, end: 5 });
    expect(mergeWithHighlight(textNode, highlight1)).toEqual([
      HTMLNode.createTagNode({
        name: 'mark',
        children: [HTMLNode.createTextNode({ data: 'he', textStartPosition: 3, textEndPosition: 5 })],
      }),
      HTMLNode.createTextNode({ data: 'y!', textStartPosition: 5, textEndPosition: 7 }),
    ]);

    const highlight2 = Highlight.create({ start: 5, end: 98 });
    expect(mergeWithHighlight(textNode, highlight2)).toEqual([
      HTMLNode.createTextNode({ data: 'he', textStartPosition: 3, textEndPosition: 5 }),
      HTMLNode.createTagNode({
        name: 'mark',
        children: [HTMLNode.createTextNode({ data: 'y!', textStartPosition: 5, textEndPosition: 7 })],
      }),
    ]);
  });
});
