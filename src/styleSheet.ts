export type TagStyle = { [name: string]: string | number | boolean | undefined };

export type StyleSheet = Record<string, TagStyle>;

export const StyleSheet = (() => {
  function stringify(styleSheet: StyleSheet): string {
    return (
      Object.keys(styleSheet)
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        .map((selector) => `${selector} {${stringifyStyle(styleSheet[selector]!)}}`)
        .join('')
    );
  }

  function stringifyStyle(tagStyle: TagStyle): string {
    return Object.keys(tagStyle).reduce((returnValue, attribName) => {
      const camelCaseAttrib = attribName.replace(/([A-Z])/g, (_, char) => `-${String(char).toLowerCase()}`);
      const value = tagStyle[attribName];
      const attribString =
        value != null
          ? `${camelCaseAttrib}:${typeof value === 'number' ? `${Math.round(value).toFixed(3)}px` : String(value)}`
          : '';

      return attribString.length === 0
        ? returnValue
        : returnValue.length === 0
        ? attribString
        : `${returnValue};${attribString}`;
    }, '');
  }

  return {
    stringify,
  };
})();
