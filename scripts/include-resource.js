const fs = require('fs').promises;
const path = require('path');

/**
 *
 * @param {string} content
 * @param {string} escapedCharacters
 */
function escapeCharacters(content, escapedCharacters) {
  return String(content).replace(new RegExp(`\\\\([\\S\\s])|([${escapedCharacters}])`, 'g'), '\\$1$2');
}

/**
 *
 * @param {string} scriptContent
 */
function htmlDocument(scriptContent) {
  function declareCommonJS(commonJSModule, isMain = false) {
    return `(function (require, module, exports) {
  ${commonJSModule}
}).apply(this, __createContext(${Boolean(isMain)}));`;
  }

  return `<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
  <script>
(function () {
  function __createContext(main) {
    const require = function require(id) {
      throw new Error('NotImplemented');
    };
    require.main = main;
    const module = { exports: {} };
    return [require, module, module.exports];
  }

  ${declareCommonJS(scriptContent, true)}
}).call(this);
    </script>
  </body>
</html>`;
}

/**
 *
 * @param {string} htmlContent
 */
function htmlToTSModule(htmlContent) {
  return `export default \`
  ${escapeCharacters(htmlContent, '`$')}
  \`;\n`;
}

async function main() {
  const rootDir = path.dirname(__dirname);
  const sourceFile = path.join(rootDir, 'resource', 'main.js');
  const destinationFile = path.join(rootDir, 'src', 'webView.ts');

  const sourceContent = (await fs.readFile(sourceFile)).toString('utf8');
  const destinationContent = htmlToTSModule(htmlDocument(sourceContent));
  await fs.writeFile(destinationFile, destinationContent);
}

main();
