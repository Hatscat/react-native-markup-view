// eslint-disable-next-line import/no-unassigned-import
import { postAction, main, handleSelectionChange } from './main';

jest.mock('./main', () => ({
  // @ts-ignore
  ...jest.requireActual('./main'),
  postAction: jest.fn(),
}));

const _window = /** @type {Window & typeof globalThis & {onWebViewMessage: (event: string) => void}} */ (window);

describe('onWebViewMessage()', () => {
  test('should be a function', () => {
    main();
    expect(typeof _window.onWebViewMessage).toBe('function');
  });
  test('DOMUpdate, multiple node inserts in linear order should be correctly displayed', () => {
    main();
    // <div><p>foo</p><p>bar</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'div', node: { name: 'div', childIndex: 0 } },
          { op: 'insert', id: 'p1', node: { name: 'p', parentId: 'div', childIndex: 0 } },
          { op: 'insert', id: 'text:foo', node: { data: 'foo', parentId: 'p1', childIndex: 0 } },
          { op: 'insert', id: 'p2', node: { name: 'p', parentId: 'div', childIndex: 1 } },
          { op: 'insert', id: 'text:bar', node: { data: 'bar', parentId: 'p2', childIndex: 0 } },
        ],
      })
    );
    // add a text child at the end : <div><p>foo</p><p>bar</p>foobar</div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [{ op: 'insert', id: 'text:foobar', node: { data: 'foobar', parentId: 'div', childIndex: 2 } }],
      })
    );
    expect(window.document.body.innerHTML).toEqual(
      '<div id="div"><p id="p1"><span id="text:foo">foo</span></p><p id="p2"><span id="text:bar">bar</span></p><span id="text:foobar">foobar</span></div>'
    );
  });
  test('DOMUpdate, node insert at the middle of parent should be correctly displayed', () => {
    main();
    // <div><p>foo</p><p>bar</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'div', node: { name: 'div', childIndex: 0 } },
          { op: 'insert', id: 'p1', node: { name: 'p', parentId: 'div', childIndex: 0 } },
          { op: 'insert', id: 'text:foo', node: { data: 'foo', parentId: 'p1', childIndex: 0 } },
          { op: 'insert', id: 'p2', node: { name: 'p', parentId: 'div', childIndex: 1 } },
          { op: 'insert', id: 'text:bar', node: { data: 'bar', parentId: 'p2', childIndex: 0 } },
        ],
      })
    );
    // add a text child : <div><p>foo</p>foobar<p>bar</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [{ op: 'insert', id: 'text:foobar', node: { data: 'foobar', parentId: 'div', childIndex: 1 } }],
      })
    );
    expect(window.document.body.innerHTML).toEqual(
      '<div id="div"><p id="p1"><span id="text:foo">foo</span></p><span id="text:foobar">foobar</span><p id="p2"><span id="text:bar">bar</span></p></div>'
    );
  });
  test('DOMUpdate, node insert at specific index of root document should be correctly displayed', () => {
    main();
    // <p>foo</p><p>bar</p>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'p1', node: { name: 'p', childIndex: 0 } },
          { op: 'insert', id: 'text:foo', node: { data: 'foo', parentId: 'p1', childIndex: 0 } },
          { op: 'insert', id: 'p2', node: { name: 'p', childIndex: 1 } },
          { op: 'insert', id: 'text:bar', node: { data: 'bar', parentId: 'p2', childIndex: 0 } },
        ],
      })
    );
    // add text children : foobar<p>foo</p>foobaz<p>bar</p>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'text:foobar', node: { data: 'foobar', childIndex: 0 } },
          { op: 'insert', id: 'text:foobaz', node: { data: 'foobaz', childIndex: 2 } },
        ],
      })
    );
    expect(window.document.body.innerHTML).toEqual(
      '<span id="text:foobar">foobar</span><p id="p1"><span id="text:foo">foo</span></p><span id="text:foobaz">foobaz</span><p id="p2"><span id="text:bar">bar</span></p>'
    );
  });
  test('DOMUpdate, complex node insert at specific index of root document should be correctly displayed', () => {
    main();
    // <div><p>foo</p><fieldset><legend>bar</legend></fieldset></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'div', node: { name: 'div', childIndex: 0 } },
          { op: 'insert', id: 'p', node: { name: 'p', parentId: 'div', childIndex: 0 } },
          {
            op: 'insert',
            id: 'fieldset',
            node: { name: 'fieldset', parentId: 'div', childIndex: 1, attribs: { id: 'aaa' } },
          },
          { op: 'insert', id: 'text:foo', node: { data: 'foo', parentId: 'p', childIndex: 0 } },
          { op: 'insert', id: 'legend', node: { name: 'legend', parentId: 'fieldset', childIndex: 0 } },
          { op: 'insert', id: 'text:bar', node: { data: 'bar', parentId: 'legend', childIndex: 0 } },
        ],
      })
    );
    expect(window.document.body.innerHTML).toEqual(
      '<div id="div"><p id="p"><span id="text:foo">foo</span></p><fieldset id="fieldset"><legend id="legend"><span id="text:bar">bar</span></legend></fieldset></div>'
    );
  });
  test('DOMUpdate, multiple node insert at the beginning of parent should be correctly displayed', () => {
    main();
    // <div><p>foo</p><p>bar</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'div', node: { name: 'div', childIndex: 0 } },
          { op: 'insert', id: 'p1', node: { name: 'p', parentId: 'div', childIndex: 0 } },
          { op: 'insert', id: 'text:foo', node: { data: 'foo', parentId: 'p1', childIndex: 0 } },
          { op: 'insert', id: 'p2', node: { name: 'p', parentId: 'div', childIndex: 1 } },
          { op: 'insert', id: 'text:bar', node: { data: 'bar', parentId: 'p2', childIndex: 0 } },
        ],
      })
    );
    // add a text child at the begin : <div>foobar<p>foo</p><p>bar</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [{ op: 'insert', id: 'text:foobar', node: { data: 'foobar', parentId: 'div', childIndex: 0 } }],
      })
    );
    expect(window.document.body.innerHTML).toEqual(
      '<div id="div"><span id="text:foobar">foobar</span><p id="p1"><span id="text:foo">foo</span></p><p id="p2"><span id="text:bar">bar</span></p></div>'
    );
  });
  test('DOMUpdate, update node position is currently not supported', () => {
    main();
    // <div><p>foo</p><p>bar</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [
          { op: 'insert', id: 'div', node: { name: 'div', childIndex: 0 } },
          { op: 'insert', id: 'p1', node: { name: 'p', parentId: 'div', childIndex: 0 } },
          { op: 'insert', id: 'text:foo', node: { data: 'foo', parentId: 'p1', childIndex: 0 } },
          { op: 'insert', id: 'p2', node: { name: 'p', parentId: 'div', childIndex: 1 } },
          { op: 'insert', id: 'text:bar', node: { data: 'bar', parentId: 'p2', childIndex: 0 } },
        ],
      })
    );
    // move "bar" before "foo" <div><p>bar</p><p>foo</p></div>
    _window.onWebViewMessage(
      JSON.stringify({
        type: 'DOMUpdate',
        diff: [{ op: 'update', id: 'text:bar', node: { data: 'bar', parentId: 'div', childIndex: 0 } }],
      })
    );
    expect(window.document.body.innerHTML).not.toEqual(
      '<div id="div"><p id="p2"><span id="text:bar">bar</span></p><p id="p1"><span id="text:foo">foo</span></div>'
    );
  });
});
describe('handleSelectionChange()', () => {
  test.skip('should call postAction({type: selection})', () => {
    jest.spyOn(document, 'getSelection').mockReturnValue(null);

    handleSelectionChange();

    expect(postAction).toHaveBeenCalledWith({
      type: 'selection',
      selection: undefined,
    });
  });
});
