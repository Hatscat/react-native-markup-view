const fs = require('fs').promises;
const path = require('path');

const fontByExt = {
  'woff': {
    data: 'application/font-woff',
    format: 'woff',
  },
  'woff2': {
    data: 'application/font-woff2',
    format: 'woff2',
  },
  'eot': {
    data: 'application/vnd.ms-fontobject',
    format: 'embedded-opentype'
  },
  'ttf': {
    data: 'font/truetype',
    format: 'truetype',
  },
  'otf': {
    data: 'font/opentype',
    format: 'opentype',
  },
  'svg': {
    data: 'image/svg+xml',
    format: 'svg',
  }
};

/**
 * @param {{name: string, url: string} | null} font
 * @returns {string}
 */
function fontStringMapper(font) {
  return font ? `  ${font.name}: \`${font.url}\`,\n` : ''
}

/**
 * @param {({name: string, url: string} | null)[]} fonts
 * @returns {string}
 */
function fontNameUnionType(fonts) {
  return fonts.filter(Boolean).map(font => `'${font.name}'`).join(' | ');
}

async function main() {
  const rootDir = path.dirname(__dirname);
  const fontsDir = path.join(rootDir, 'assets');
  const destinationFile = path.join(rootDir, 'src', 'fonts.ts');

  const fontFiles = await fs.readdir(fontsDir);

  const fonts = await Promise.all(fontFiles.map(async file => {
    const fontPath = path.join(fontsDir, file);
    const fontString = await fs.readFile(fontPath);
    const content = Buffer.from(fontString).toString('base64');
    const [ext, name] = file.split('.').reverse();
    const fontInfos = fontByExt[ext];

    return fontInfos ? {
      name,
      url: `url(data:${fontInfos.data};charset=utf-8;base64,${content}) format('${fontInfos.format}')`
    } : null;
  }));

  await fs.writeFile(destinationFile, `export default {
${fonts.map(fontStringMapper).join('')}} as Record<${fontNameUnionType(fonts)}, string>;`);
}

main();
