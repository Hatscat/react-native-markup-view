import type { ContentTextSource } from '@koober/react-native-markup-view'

function html(content: string) {
  return {
    encoding: 'text/html' as const,
    content
  };
}

function plain(content: string) {
  return {
    encoding: 'text/plain' as const,
    content
  };
}

function snippet(name: string, content: ContentTextSource | ContentTextSource[]) {
  return {
    snippetName: name,
    snippetContent: content
  };
}

export const plainText = snippet('Plain Text', plain(`
This is first line.
This is second line with a <tag /> that should&nbsp;not be interpreted.
`));

export const simple = snippet('Simple', html(`
<h1 style="font-size:3rem">non breaking&nbsp;space</h1>
here is some quote marks : ' ' " " \` \` “ ” ‘ ’ « » ‹ ›
<div style="background-color: #CFC">
<p style="font-size:1.3em;">This paragraph is styled a font size set in em !</p>
<em>This one showcases the default renderer for the "em" HTML tag.</em>
<p style="padding:10%;">This one features a padding <strong style="margin: 10%; padding:20%">in percentage !</strong></p>
<hr />
<i>Here, we have a style set on the "i" tag with the "styleSheet" prop.</i>
</div>
<p>And <a href="https://developer.mozilla.org" title="Google FR">This is a link !</a></p>
<div style="background-color: red; height: 100px; width:100px; margin-left:auto;"></div>
<a href="https://developer.mozilla.org"><img src="https://developer.mozilla.org/static/img/favicon144.png" height="400" width="400" style="display:block; margin:auto; width:50vw; height:50vw;" alt="Visiter le site MDN"></a>
`));

export const hardCase = snippet('hardCase', html(`
<p>
    <!--[if gte mso 9]><xml>     <o:AllowPNG/>   96   </xml><![endif]-->
    <!--[if gte mso 9]><xml>  <w:WordDocument>   <w:View>Normal</w:View>   <w:Zoom>0</w:Zoom>   <w:TrackMoves/>   <w:TrackFormatting/>   <w:HyphenationZone>21</w:HyphenationZone>   <w:PunctuationKerning/>   <w:ValidateAgainstSchemas/>   <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>   <w:IgnoreMixedContent>false</w:IgnoreMixedContent>   <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>   <w:DoNotPromoteQF/>   <w:LidThemeOther>FR</w:LidThemeOther>   <w:LidThemeAsian>X-NONE</w:LidThemeAsian>   <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>   <w:Compatibility>    <w:BreakWrappedTables/>    <w:SnapToGridInCell/>    <w:WrapTextWithPunct/>    <w:UseAsianBreakRules/>    <w:DontGrowAutofit/>    <w:SplitPgBreakAndParaMark/>    <w:EnableOpenTypeKerning/>    <w:DontFlipMirrorIndents/>    <w:OverrideTableStyleHps/>   </w:Compatibility>   <m:mathPr>    <m:mathFont m:val="Cambria Math"/>    <m:brkBin m:val="before"/>    <m:brkBinSub m:val="&#45;-"/>    <m:smallFrac m:val="off"/>    <m:dispDef/>    <m:lMargin m:val="0"/>    <m:rMargin m:val="0"/>    <m:defJc m:val="centerGroup"/>    <m:wrapIndent m:val="1440"/>    <m:intLim m:val="subSup"/>    <m:naryLim m:val="undOvr"/>   </m:mathPr></w:WordDocument> </xml><![endif]-->
    <!--[if gte mso 9]><xml>  <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"   DefSemiHidden="false" DefQFormat="false" DefPriority="99"   LatentStyleCount="380">   <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>   <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>   <w:LsdException Locked="false" Priority="9" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 6"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 7"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 8"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index 9"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 1"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 2"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 3"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 4"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 5"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 6"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 7"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 8"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" Name="toc 9"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Normal Indent"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="footnote text"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="annotation text"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="header"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="footer"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="index heading"/>   <w:LsdException Locked="false" Priority="35" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="caption"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="table of figures"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="envelope address"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="envelope return"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="footnote reference"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="annotation reference"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="line number"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="page number"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="endnote reference"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="endnote text"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="table of authorities"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="macro"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="toa heading"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Bullet"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Number"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Bullet 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Bullet 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Bullet 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Bullet 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Number 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Number 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Number 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Number 5"/>   <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Closing"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Signature"/>   <w:LsdException Locked="false" Priority="1" SemiHidden="true"    UnhideWhenUsed="true" Name="Default Paragraph Font"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text Indent"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Continue"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Continue 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Continue 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Continue 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="List Continue 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Message Header"/>   <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Salutation"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Date"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text First Indent"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text First Indent 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Heading"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text Indent 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Body Text Indent 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Block Text"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Hyperlink"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="FollowedHyperlink"/>   <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>   <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Document Map"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Plain Text"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="E-mail Signature"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Top of Form"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Bottom of Form"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Normal (Web)"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Acronym"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Address"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Cite"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Code"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Definition"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Keyboard"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Preformatted"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Sample"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Typewriter"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="HTML Variable"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Normal Table"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="annotation subject"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="No List"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Outline List 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Outline List 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Outline List 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Simple 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Simple 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Simple 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Classic 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Classic 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Classic 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Classic 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Colorful 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Colorful 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Colorful 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Columns 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Columns 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Columns 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Columns 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Columns 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 6"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 7"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Grid 8"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 6"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 7"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table List 8"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table 3D effects 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table 3D effects 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table 3D effects 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Contemporary"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Elegant"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Professional"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Subtle 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Subtle 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Web 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Web 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Web 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Balloon Text"/>   <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Table Theme"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 1"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 2"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 3"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 4"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 5"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 6"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 7"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 8"/>   <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"    Name="Note Level 9"/>   <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>   <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>   <w:LsdException Locked="false" Priority="61" Name="Light List"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>   <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>   <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>   <w:LsdException Locked="false" Priority="34" QFormat="true"    Name="List Paragraph"/>   <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>   <w:LsdException Locked="false" Priority="30" QFormat="true"    Name="Intense Quote"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>   <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>   <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>   <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>   <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>   <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>   <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>   <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>   <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>   <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>   <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>   <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>   <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>   <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>   <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>   <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>   <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>   <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>   <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>   <w:LsdException Locked="false" Priority="19" QFormat="true"    Name="Subtle Emphasis"/>   <w:LsdException Locked="false" Priority="21" QFormat="true"    Name="Intense Emphasis"/>   <w:LsdException Locked="false" Priority="31" QFormat="true"    Name="Subtle Reference"/>   <w:LsdException Locked="false" Priority="32" QFormat="true"    Name="Intense Reference"/>   <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>   <w:LsdException Locked="false" Priority="37" SemiHidden="true"    UnhideWhenUsed="true" Name="Bibliography"/>   <w:LsdException Locked="false" Priority="39" SemiHidden="true"    UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>   <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>   <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>   <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>   <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>   <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>   <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>   <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>   <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>   <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>   <w:LsdException Locked="false" Priority="46"    Name="Grid Table 1 Light Accent 1"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>   <w:LsdException Locked="false" Priority="51"    Name="Grid Table 6 Colorful Accent 1"/>   <w:LsdException Locked="false" Priority="52"    Name="Grid Table 7 Colorful Accent 1"/>   <w:LsdException Locked="false" Priority="46"    Name="Grid Table 1 Light Accent 2"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>   <w:LsdException Locked="false" Priority="51"    Name="Grid Table 6 Colorful Accent 2"/>   <w:LsdException Locked="false" Priority="52"    Name="Grid Table 7 Colorful Accent 2"/>   <w:LsdException Locked="false" Priority="46"    Name="Grid Table 1 Light Accent 3"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>   <w:LsdException Locked="false" Priority="51"    Name="Grid Table 6 Colorful Accent 3"/>   <w:LsdException Locked="false" Priority="52"    Name="Grid Table 7 Colorful Accent 3"/>   <w:LsdException Locked="false" Priority="46"    Name="Grid Table 1 Light Accent 4"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>   <w:LsdException Locked="false" Priority="51"    Name="Grid Table 6 Colorful Accent 4"/>   <w:LsdException Locked="false" Priority="52"    Name="Grid Table 7 Colorful Accent 4"/>   <w:LsdException Locked="false" Priority="46"    Name="Grid Table 1 Light Accent 5"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>   <w:LsdException Locked="false" Priority="51"    Name="Grid Table 6 Colorful Accent 5"/>   <w:LsdException Locked="false" Priority="52"    Name="Grid Table 7 Colorful Accent 5"/>   <w:LsdException Locked="false" Priority="46"    Name="Grid Table 1 Light Accent 6"/>   <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>   <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>   <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>   <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>   <w:LsdException Locked="false" Priority="51"    Name="Grid Table 6 Colorful Accent 6"/>   <w:LsdException Locked="false" Priority="52"    Name="Grid Table 7 Colorful Accent 6"/>   <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>   <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>   <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>   <w:LsdException Locked="false" Priority="46"    Name="List Table 1 Light Accent 1"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>   <w:LsdException Locked="false" Priority="51"    Name="List Table 6 Colorful Accent 1"/>   <w:LsdException Locked="false" Priority="52"    Name="List Table 7 Colorful Accent 1"/>   <w:LsdException Locked="false" Priority="46"    Name="List Table 1 Light Accent 2"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>   <w:LsdException Locked="false" Priority="51"    Name="List Table 6 Colorful Accent 2"/>   <w:LsdException Locked="false" Priority="52"    Name="List Table 7 Colorful Accent 2"/>   <w:LsdException Locked="false" Priority="46"    Name="List Table 1 Light Accent 3"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>   <w:LsdException Locked="false" Priority="51"    Name="List Table 6 Colorful Accent 3"/>   <w:LsdException Locked="false" Priority="52"    Name="List Table 7 Colorful Accent 3"/>   <w:LsdException Locked="false" Priority="46"    Name="List Table 1 Light Accent 4"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>   <w:LsdException Locked="false" Priority="51"    Name="List Table 6 Colorful Accent 4"/>   <w:LsdException Locked="false" Priority="52"    Name="List Table 7 Colorful Accent 4"/>   <w:LsdException Locked="false" Priority="46"    Name="List Table 1 Light Accent 5"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>   <w:LsdException Locked="false" Priority="51"    Name="List Table 6 Colorful Accent 5"/>   <w:LsdException Locked="false" Priority="52"    Name="List Table 7 Colorful Accent 5"/>   <w:LsdException Locked="false" Priority="46"    Name="List Table 1 Light Accent 6"/>   <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>   <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>   <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>   <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>   <w:LsdException Locked="false" Priority="51"    Name="List Table 6 Colorful Accent 6"/>   <w:LsdException Locked="false" Priority="52"    Name="List Table 7 Colorful Accent 6"/>  </w:LatentStyles> </xml><![endif]-->
    <!--[if gte mso 10]> <style>  /* Style Definitions */ table.MsoNormalTable 	{mso-style-name:"Tableau Normal"; 	mso-tstyle-rowband-size:0; 	mso-tstyle-colband-size:0; 	mso-style-noshow:yes; 	mso-style-priority:99; 	mso-style-parent:""; 	mso-padding-alt:0cm 5.4pt 0cm 5.4pt; 	mso-para-margin:0cm; 	mso-para-margin-bottom:.0001pt; 	mso-pagination:widow-orphan; 	font-size:12.0pt; 	font-family:Calibri; 	mso-ascii-font-family:Calibri; 	mso-ascii-theme-font:minor-latin; 	mso-hansi-font-family:Calibri; 	mso-hansi-theme-font:minor-latin; 	mso-fareast-language:EN-US;} </style> <![endif]-->
</p>
<p>fhdkshfksdjhfksdjhfjksh fsdh ksdfhf kjsd fdsjkfhjsdhfk jshdjf hsdjfhk sdhfj shdfdhsjfhdks jhfk sdhfk sdhfk</p>

<p><b>dklsjdkqls lskq jdlkq j : </b></p>

<p>
    <!--[if !supportLists]-->·
    <!--[endif]-->djlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqd</p>

<p>
    <!--[if !supportLists]-->·
    <!--[endif]-->djlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqd</p>

<p>
    <!--[if !supportLists]-->·
    <!--[endif]-->djlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqd</p>

<p>
    <!--[if !supportLists]-->·
    <!--[endif]-->djlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqd</p>

<p>djlskqjdlqsjd kjsqdlk&nbsp;jqslkdj&nbsp;lkqsjd&nbsp;lkjqslkj&nbsp;qsdlkj&nbsp;lksqddjlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqddjlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqddjlskqjdlqsjd kjsqdlk jqslkdj lkqsjd lkjqslkj qsdlkj lksqd&nbsp;</p>
`));


const allTags = snippet('All Tags', html(`
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HTML5 Test Page</title>
  </head>
  <body>
    <div id="top" class="page" role="document">
      <header role="banner">
        <h1>HTML5 Test Page</h1>
        <p>This is a test page filled with common HTML elements to be used to provide visual feedback whilst building CSS systems and frameworks.</p>
      </header>
      <nav role="navigation">
        <ul>
          <li>
            <a href="#text">Text</a>
            <ul>
              <li><a href="#text__headings">Headings</a></li>
              <li><a href="#text__paragraphs">Paragraphs</a></li>
              <li><a href="#text__blockquotes">Blockquotes</a></li>
              <li><a href="#text__lists">Lists</a></li>
              <li><a href="#text__hr">Horizontal rules</a></li>
              <li><a href="#text__tables">Tabular data</a></li>
              <li><a href="#text__code">Code</a></li>
              <li><a href="#text__inline">Inline elements</a></li>
              <li><a href="#text__comments">HTML Comments</a></li>
            </ul>
          </li>
          <li>
            <a href="#embedded">Embedded content</a>
            <ul>
              <li><a href="#embedded__images">Images</a></li>
              <li><a href="#embedded__audio">Audio</a></li>
              <li><a href="#embedded__video">Video</a></li>
              <li><a href="#embedded__canvas">Canvas</a></li>
              <li><a href="#embedded__meter">Meter</a></li>
              <li><a href="#embedded__progress">Progress</a></li>
              <li><a href="#embedded__svg">Inline SVG</a></li>
              <li><a href="#embedded__iframe">IFrames</a></li>
            </ul>
          </li>
          <li>
            <a href="#forms">Form elements</a>
            <ul>
              <li><a href="#forms__input">Input fields</a></li>
              <li><a href="#forms__select">Select menus</a></li>
              <li><a href="#forms__checkbox">Checkboxes</a></li>
              <li><a href="#forms__radio">Radio buttons</a></li>
              <li><a href="#forms__textareas">Textareas</a></li>
              <li><a href="#forms__html5">HTML5 inputs</a></li>
              <li><a href="#forms__action">Action buttons</a></li>
            </ul>
          </li>
        </ul>
      </nav>
      <main role="main">
        <section id="text">
          <header><h1>Text</h1></header>
          <article id="text__headings">
            <header>
              <h1>Headings</h1>
            </header>
            <div>
              <h1>Heading 1<sub>sub</sub><sup>sup</sup></h1>
              <h2>Heading 2<sub>sub</sub><sup>sup</sup></h2>
              <h3>Heading 3<sub>sub</sub><sup>sup</sup></h3>
              <h4>Heading 4<sub>sub</sub><sup>sup</sup></h4>
              <h5>Heading 5<sub>sub</sub><sup>sup</sup></h5>
              <h6>Heading 6<sub>sub</sub><sup>sup</sup></h6>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__paragraphs">
            <header><h1>Paragraphs</h1></header>
            <div>
              <p>A paragraph (from the Greek paragraphos, “to write beside” or “written beside”) is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences. Though not required by the syntax of any language, paragraphs are usually an expected part of formal writing, used to organize longer prose.</p>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__blockquotes">
            <header><h1>Blockquotes</h1></header>
            <div>
              <blockquote>
                <p>A block quotation (also known as a long quotation or extract) is a quotation in a written document, that is set off from the main text as a paragraph, or block of text.</p>
                <p>It is typically distinguished visually using indentation and a different typeface or smaller size quotation. It may or may not include a citation, usually placed at the bottom.</p>
                <cite><a href="#!">Said no one, ever.</a></cite>
              </blockquote>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__lists">
            <header><h1>Lists</h1></header>
            <div>
              <h3>Definition list</h3>
              <dl>
                <dt>Definition List Title</dt>
                <dd>This is a definition list division.</dd>
              </dl>
              <h3>Ordered List</h3>
              <ol>
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
              </ol>
              <h3>Unordered List</h3>
              <ul>
                <li>List Item 1</li>
                <li>List Item 2</li>
                <li>List Item 3</li>
              </ul>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__hr">
            <header><h1>Horizontal rules</h1></header>
            <div>
              <hr>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__tables">
            <header><h1>Tabular data</h1></header>
            <table>
              <caption>Table Caption</caption>
              <thead>
                <tr>
                  <th>Table Heading 1</th>
                  <th>Table Heading 2</th>
                  <th>Table Heading 3</th>
                  <th>Table Heading 4</th>
                  <th>Table Heading 5</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Table Footer 1</th>
                  <th>Table Footer 2</th>
                  <th>Table Footer 3</th>
                  <th>Table Footer 4</th>
                  <th>Table Footer 5</th>
                </tr>
              </tfoot>
              <tbody>
                <tr>
                  <td>Table Cell 1</td>
                  <td>Table Cell 2</td>
                  <td>Table Cell 3</td>
                  <td>Table Cell 4</td>
                  <td>Table Cell 5</td>
                </tr>
                <tr>
                  <td>Table Cell 1</td>
                  <td>Table Cell 2</td>
                  <td>Table Cell 3</td>
                  <td>Table Cell 4</td>
                  <td>Table Cell 5</td>
                </tr>
                <tr>
                  <td>Table Cell 1</td>
                  <td>Table Cell 2</td>
                  <td>Table Cell 3</td>
                  <td>Table Cell 4</td>
                  <td>Table Cell 5</td>
                </tr>
                <tr>
                  <td>Table Cell 1</td>
                  <td>Table Cell 2</td>
                  <td>Table Cell 3</td>
                  <td>Table Cell 4</td>
                  <td>Table Cell 5</td>
                </tr>
              </tbody>
            </table>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__code">
            <header><h1>Code</h1></header>
            <div>
              <p><strong>Keyboard input:</strong> <kbd>Cmd</kbd></p>
              <p><strong>Inline code:</strong> <code>&lt;div&gt;code&lt;/div&gt;</code></p>
              <p><strong>Sample output:</strong> <samp>This is sample output from a computer program.</samp></p>
              <h2>Pre-formatted text</h2>
              <pre>P R E F O R M A T T E D T E X T
<br>! " # $ % &amp; ' ( ) * + , - . /
<br>0 1 2 3 4 5 6 7 8 9 : ; &lt; = &gt; ?
<br>@ A B C D E F G H I J K L M N O
<br>P Q R S T U V W X Y Z [ \ ] ^ _
<br>a b c d e f g h i j k l m n o
<br>p q r s t u v w x y z { | } ~ </pre>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__inline">
            <header><h1>Inline elements</h1></header>
            <div>
              <p><a href="#!">This is a text link</a>.</p>
              <p><strong>Strong is used to indicate strong importance.</strong></p>
              <p><em>This text has added emphasis.</em></p>
              <p>The <b>b element</b> is stylistically different text from normal text, without any special importance.</p>
              <p>The <i>i element</i> is text that is offset from the normal text.</p>
              <p>The <u>u element</u> is text with an unarticulated, though explicitly rendered, non-textual annotation.</p>
              <p><del>This text is deleted</del> and <ins>This text is inserted</ins>.</p>
              <p><s>This text has a strikethrough</s>.</p>
              <p>Superscript<sup>®</sup>.</p>
              <p>Subscript for things like H<sub>2</sub>O.</p>
              <p><small>This small text is small for for fine print, etc.</small></p>
              <p>Abbreviation: <abbr title="HyperText Markup Language">HTML</abbr></p>
              <p><q cite="https://developer.mozilla.org/en-US/docs/HTML/Element/q">This text is a short inline quotation.</q></p>
              <p><cite>This is a citation.</cite></p>
              <p>The <dfn>dfn element</dfn> indicates a definition.</p>
              <p>The <mark>mark element</mark> indicates a highlight.</p>
              <p>The <var>variable element</var>, such as <var>x</var> = <var>y</var>.</p>
              <p>The time element: <time datetime="2013-04-06T12:32+00:00">2 weeks ago</time></p>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="text__comments">
            <header><h1>HTML Comments</h1></header>
            <div>
              <p>There is comment here: <!--This comment should not be displayed--></p>
              <p>There is a comment spanning multiple tags and lines below here.</p>
              <!--<p><a href="#!">This is a text link. But it should not be displayed in a comment</a>.</p>
              <p><strong>Strong is used to indicate strong importance. But, it should not be displayed in a comment</strong></p>
              <p><em>This text has added emphasis. But, it should not be displayed in a comment</em></p>-->
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
        </section>
        <section id="embedded">
          <header><h1>Embedded content</h1></header>
          <article id="embedded__images">
            <header><h2>Images</h2></header>
            <div>
              <h3>No <code>&lt;figure&gt;</code> element</h3>
              <p><img src="http://placekitten.com/480/480" alt="Image alt text"></p>
              <h3>Wrapped in a <code>&lt;figure&gt;</code> element, no <code>&lt;figcaption&gt;</code></h3>
              <figure><img src="http://placekitten.com/420/420" alt="Image alt text"></figure>
              <h3>Wrapped in a <code>&lt;figure&gt;</code> element, with a <code>&lt;figcaption&gt;</code></h3>
              <figure>
                <img src="http://placekitten.com/420/420" alt="Image alt text">
                <figcaption>Here is a caption for this image.</figcaption>
              </figure>
            </div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__audio">
            <header><h2>Audio</h2></header>
            <div><audio controls="">audio</audio></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__video">
            <header><h2>Video</h2></header>
            <div><video controls="">video</video></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__canvas">
            <header><h2>Canvas</h2></header>
            <div><canvas>canvas</canvas></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__meter">
            <header><h2>Meter</h2></header>
            <div><meter value="2" min="0" max="10">2 out of 10</meter></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__progress">
            <header><h2>Progress</h2></header>
            <div><progress>progress</progress></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__svg">
            <header><h2>Inline SVG</h2></header>
            <div><svg width="100px" height="100px"><circle cx="100" cy="100" r="100" fill="#1fa3ec"></circle></svg></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
          <article id="embedded__iframe">
            <header><h2>IFrame</h2></header>
            <div><iframe src="index.html" height="300"></iframe></div>
            <footer><p><a href="#top">[Top]</a></p></footer>
          </article>
        </section>
        <section id="forms">
          <header><h1>Form elements</h1></header>
          <form>
            <fieldset id="forms__input">
              <legend>Input fields</legend>
              <p>
                <label for="input__text">Text Input</label>
                <input id="input__text" type="text" placeholder="Text Input">
              </p>
              <p>
                <label for="input__password">Password</label>
                <input id="input__password" type="password" placeholder="Type your Password">
              </p>
              <p>
                <label for="input__webaddress">Web Address</label>
                <input id="input__webaddress" type="url" placeholder="http://yoursite.com">
              </p>
              <p>
                <label for="input__emailaddress">Email Address</label>
                <input id="input__emailaddress" type="email" placeholder="name@email.com">
              </p>
              <p>
                <label for="input__phone">Phone Number</label>
                <input id="input__phone" type="tel" placeholder="(999) 999-9999">
              </p>
              <p>
                <label for="input__search">Search</label>
                <input id="input__search" type="search" placeholder="Enter Search Term">
              </p>
              <p>
                <label for="input__text2">Number Input</label>
                <input id="input__text2" type="number" placeholder="Enter a Number">
              </p>
              <p>
                <label for="input__text3" class="error">Error</label>
                <input id="input__text3" class="is-error" type="text" placeholder="Text Input">
              </p>
              <p>
                <label for="input__text4" class="valid">Valid</label>
                <input id="input__text4" class="is-valid" type="text" placeholder="Text Input">
              </p>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
            <fieldset id="forms__select">
              <legend>Select menus</legend>
              <p>
                <label for="select">Select</label>
                <select id="select">
                  <optgroup label="Option Group">
                    <option>Option One</option>
                    <option>Option Two</option>
                    <option>Option Three</option>
                  </optgroup>
                </select>
              </p>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
            <fieldset id="forms__checkbox">
              <legend>Checkboxes</legend>
              <ul class="list list--bare">
                <li><label for="checkbox1"><input id="checkbox1" name="checkbox" type="checkbox" checked="checked"> Choice A</label></li>
                <li><label for="checkbox2"><input id="checkbox2" name="checkbox" type="checkbox"> Choice B</label></li>
                <li><label for="checkbox3"><input id="checkbox3" name="checkbox" type="checkbox"> Choice C</label></li>
              </ul>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
            <fieldset id="forms__radio">
              <legend>Radio buttons</legend>
              <ul class="list list--bare">
                <li><label for="radio1"><input id="radio1" name="radio" type="radio" class="radio" checked="checked"> Option 1</label></li>
                <li><label for="radio2"><input id="radio2" name="radio" type="radio" class="radio"> Option 2</label></li>
                <li><label for="radio3"><input id="radio3" name="radio" type="radio" class="radio"> Option 3</label></li>
              </ul>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
            <fieldset id="forms__textareas">
              <legend>Textareas</legend>
              <p>
                <label for="textarea">Textarea</label>
                <textarea id="textarea" rows="8" cols="48" placeholder="Enter your message here"></textarea>
              </p>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
            <fieldset id="forms__html5">
              <legend>HTML5 inputs</legend>
              <p>
                <label for="ic">Color input</label>
                <input type="color" id="ic" value="#000000">
              </p>
              <p>
                <label for="in">Number input</label>
                <input type="number" id="in" min="0" max="10" value="5">
              </p>
              <p>
                <label for="ir">Range input</label>
                <input type="range" id="ir" value="10">
              </p>
              <p>
                <label for="idd">Date input</label>
                <input type="date" id="idd" value="1970-01-01">
              </p>
              <p>
                <label for="idm">Month input</label>
                <input type="month" id="idm" value="1970-01">
              </p>
              <p>
                <label for="idw">Week input</label>
                <input type="week" id="idw" value="1970-W01">
              </p>
              <p>
                <label for="idt">Datetime input</label>
                <input type="datetime" id="idt" value="1970-01-01T00:00:00Z">
              </p>
              <p>
                <label for="idtl">Datetime-local input</label>
                <input type="datetime-local" id="idtl" value="1970-01-01T00:00">
              </p>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
            <fieldset id="forms__action">
              <legend>Action buttons</legend>
              <p>
                <input type="submit" value="<input type=submit>">
                <input type="button" value="<input type=button>">
                <input type="reset" value="<input type=reset>">
                <input type="submit" value="<input disabled>" disabled>
              </p>
              <p>
                <button type="submit">&lt;button type=submit&gt;</button>
                <button type="button">&lt;button type=button&gt;</button>
                <button type="reset">&lt;button type=reset&gt;</button>
                <button type="button" disabled>&lt;button disabled&gt;</button>
              </p>
            </fieldset>
            <p><a href="#top">[Top]</a></p>
          </form>
        </section>
      </main>
      <footer role="contentinfo">
        <p>Made by>@cbracco</a>. Code on <a href="http://github.com/cbracco/html5-test-page">GitHub</a>.</p>
      </footer>
    </div>
  </body>
</html>
`));

const viewPageCase = snippet('View Pager', [
  html(`<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
  <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
  <dl>
   <dt>Definition list</dt>
   <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.</dd>
   <dt>Lorem ipsum dolor sit amet</dt>
   <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.</dd>
</dl>`),
html(`<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
   <li>Vestibulum auctor dapibus neque.</li>
</ul>`),
html(`<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

<pre><code>
#header h1 a {
  display: block;
  width: 300px;
  height: 80px;
}
</code></pre>`),
html(`<ul>
<li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</li>
<li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</li>
<li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</li>
<li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</li>
</ul>
<form action="#" method="post">
    <div>
         <label for="name">Text Input:</label>
         <input type="text" name="name" id="name" value="" tabindex="1" />
    </div>

    <div>
         <h4>Radio Button Choice</h4>

         <label for="radio-choice-1">Choice 1</label>
         <input type="radio" name="radio-choice-1" id="radio-choice-1" tabindex="2" value="choice-1" />

     <label for="radio-choice-2">Choice 2</label>
         <input type="radio" name="radio-choice-2" id="radio-choice-2" tabindex="3" value="choice-2" />
    </div>

  <div>
    <label for="select-choice">Select Dropdown Choice:</label>
    <select name="select-choice" id="select-choice">
      <option value="Choice 1">Choice 1</option>
      <option value="Choice 2">Choice 2</option>
      <option value="Choice 3">Choice 3</option>
    </select>
  </div>

  <div>
    <label for="textarea">Textarea:</label>
    <textarea cols="40" rows="8" name="textarea" id="textarea"></textarea>
  </div>

  <div>
      <label for="checkbox">Checkbox:</label>
    <input type="checkbox" name="checkbox" id="checkbox" />
    </div>

  <div>
      <input type="submit" value="Submit" />
    </div>
</form>`),
html(`<ol>
<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
<li>Aliquam tincidunt mauris eu risus.</li>
<li>Vestibulum auctor dapibus neque.</li>
</ol>
<table>
  <thead>
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>
<nav>
  <ul>
    <li><a href="#">Home</a></li>
    <li><a href="#">About</a></li>
    <li><a href="#">Clients</a></li>
    <li><a href="#">Contact Us</a></li>
  </ul>
</nav>`),
html(`<nav>
<ul>
  <li><a href="#nowhere" title="Lorum ipsum dolor sit amet">Lorem</a></li>
  <li><a href="#nowhere" title="Aliquam tincidunt mauris eu risus">Aliquam</a></li>
  <li><a href="#nowhere" title="Morbi in sem quis dui placerat ornare">Morbi</a></li>
  <li><a href="#nowhere" title="Praesent dapibus, neque id cursus faucibus">Praesent</a></li>
  <li><a href="#nowhere" title="Pellentesque fermentum dolor">Pellentesque</a></li>
</ul>
</nav>`),
html(`<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent pretium ornare mi, id ultrices magna egestas eget. Sed eu risus facilisis, venenatis justo nec, mattis leo. Suspendisse nec dolor lorem. Aliquam non blandit urna. Ut in erat suscipit, efficitur felis non, tempor lacus. Mauris eget ante ultrices, rhoncus justo eu, laoreet risus. Quisque bibendum eget sem vel aliquam. Mauris ut eros magna.
</p>
<p>
Morbi hendrerit metus ut tincidunt ultrices. Cras malesuada viverra arcu, at consequat augue lobortis sed. Sed accumsan fermentum diam in scelerisque. Phasellus ullamcorper nunc vitae magna aliquam, vitae laoreet neque maximus. Quisque porttitor tortor nibh, sed hendrerit justo accumsan id. Phasellus consectetur tempus volutpat. Donec eu facilisis eros. Phasellus ante ligula, tristique non sollicitudin malesuada, ornare ultricies arcu. Donec pulvinar quis erat eu fermentum. Aliquam aliquet sit amet justo eu pretium. Suspendisse rhoncus bibendum elit, eget cursus orci porttitor sed. Quisque ac libero eu nisl pellentesque laoreet in quis quam. In in nunc non metus feugiat commodo. Nulla mollis enim lorem, sit amet ullamcorper nisi laoreet quis. Vivamus magna sapien, vehicula id diam quis, rutrum commodo enim.
</p>
<p>
Ut justo libero, mattis eget malesuada in, vulputate vel dui. Vestibulum blandit imperdiet vehicula. Sed massa nisi, bibendum hendrerit ultricies eget, semper non lorem. Sed posuere erat eu tellus gravida, vitae aliquet ligula vulputate. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent eu lobortis tellus. Proin dignissim odio neque, sit amet lobortis eros ultrices sit amet. Suspendisse interdum risus sit amet ligula gravida iaculis. Sed iaculis maximus arcu eu ullamcorper.
</p>
<p>
Donec non vulputate purus. Etiam ultricies lacus a neque malesuada, sed rhoncus eros accumsan. Suspendisse potenti. Morbi cursus turpis ac felis fermentum, id rutrum tellus lobortis. Vestibulum accumsan id dolor volutpat fringilla. Proin id ullamcorper sem. Pellentesque vehicula, dui et ornare mollis, urna mi eleifend urna, vel porttitor tellus nisi non mi. Aliquam porta iaculis cursus. Cras ornare, nibh faucibus suscipit imperdiet, lacus nisi placerat lacus, id consequat est lacus eu purus. Vestibulum eget dolor mattis, viverra lectus at, posuere nisi. Nunc maximus dolor congue, scelerisque mauris ac, laoreet diam. Vestibulum imperdiet ipsum sem, eu ultricies arcu posuere in. Vivamus eleifend sapien at ante venenatis ultricies. Nunc vel magna justo. Nam fermentum magna ante, vel suscipit augue dapibus a.
</p>
<p>
Phasellus eget lacinia metus, a pulvinar odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum lacinia tincidunt sapien quis gravida. Donec tristique ullamcorper sem nec facilisis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed pulvinar et lectus ac pellentesque. Vestibulum aliquet justo nulla, ut scelerisque justo fringilla at.
</p>
<p>
Morbi porttitor euismod diam vitae mattis. Fusce pretium purus quis dui dignissim, ut posuere massa vestibulum. Donec quis ex quis risus volutpat iaculis. Ut maximus non libero ac vestibulum. Pellentesque nec consectetur tellus. Nunc est est, fringilla vitae maximus sed, laoreet sit amet nisi. Nam tempor id tortor ac pellentesque. Aenean et consequat neque. Pellentesque vestibulum fringilla massa, quis lobortis dolor porttitor at. Morbi interdum congue turpis, et rhoncus dui faucibus non. Aliquam tempor tincidunt orci blandit iaculis. Suspendisse cursus feugiat lorem a convallis. Aliquam sollicitudin ante sem, ac sodales nisi vulputate eu. Vestibulum fermentum posuere lectus. Ut feugiat nunc condimentum augue malesuada, mollis vehicula risus rutrum. Aenean sit amet elementum enim.
</p>
<p>
Quisque lacinia lectus vel blandit lacinia. Fusce mattis dignissim magna, quis efficitur augue aliquet id. Fusce sed justo mattis, molestie nisi eget, accumsan massa. Proin dictum augue urna, at cursus metus efficitur in. Nunc purus leo, efficitur vitae risus a, elementum pretium augue. Proin nec nibh elementum, pulvinar odio vitae, dictum eros. Nullam turpis ligula, laoreet efficitur vulputate eu, dictum sit amet ipsum. Proin libero tortor, pellentesque sed est non, efficitur congue eros. Fusce eu dapibus massa.
</p>
<p>
Etiam varius, nisl in condimentum imperdiet, tellus sapien venenatis sapien, ac porta massa enim at elit. Praesent condimentum tellus dolor, in cursus dui aliquam cursus. Mauris in ullamcorper est. Quisque vel elit felis. Maecenas a convallis mi. Vivamus faucibus porta nibh a efficitur. Cras quis odio in eros laoreet fringilla. Nunc sed eros ex. Nulla commodo quam sem, vitae eleifend ipsum consequat in. Cras venenatis nec neque eu congue. Duis sed dolor ligula.
</p>
<p>
Integer tempus non massa ornare sagittis. Integer vel porttitor augue. Morbi pellentesque placerat sollicitudin. Maecenas sodales, libero nec suscipit tincidunt, ante velit sodales purus, convallis eleifend mi lacus id augue. Nam mollis congue sapien, quis aliquet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla tempus augue eget metus efficitur, sed ultricies urna placerat. Donec et egestas orci, ut lacinia est. Integer luctus nisi eu urna fermentum, quis sagittis metus tincidunt. Phasellus blandit ut nisi quis pharetra. Vestibulum non dictum eros.
</p>
<p>
Donec blandit ac eros vitae tincidunt. Aliquam eu eros finibus, posuere nisi eu, vehicula neque. Phasellus consectetur ex nisl, auctor scelerisque nisl consequat vel. Phasellus blandit tempus lorem ut ultricies. Suspendisse id vestibulum lacus, eu ultricies ex. Fusce lacus lacus, mattis id ante ac, laoreet commodo mi. Cras ac ex luctus diam posuere faucibus placerat id magna. Duis nec nisl nibh. Aliquam at velit molestie, eleifend enim in, pretium odio. Aenean tortor neque, interdum at convallis eget, elementum sollicitudin urna. Morbi eget sodales ex. Vestibulum diam odio, ornare eget imperdiet et, lobortis non neque. Nam sapien turpis, mollis at ipsum et, faucibus pharetra tortor.
</p>
<p>
Praesent vitae ullamcorper nunc. Pellentesque odio ligula, vehicula non hendrerit lobortis, ultricies ac nunc. Praesent efficitur eros nibh, vel gravida augue suscipit sed. Curabitur at dui sit amet ante fringilla malesuada volutpat eu lorem. Nulla eu lectus hendrerit, finibus risus sit amet, ullamcorper ipsum. Mauris ex felis, dapibus non magna nec, venenatis placerat arcu. Fusce elementum nulla consectetur elit posuere pharetra. Curabitur cursus, nibh in congue ornare, dolor nulla tincidunt nisi, at tempor sapien nisi ac est. Aenean tincidunt ligula orci, nec vulputate lacus molestie vitae. Phasellus at tellus at arcu convallis dapibus non in neque. Nunc volutpat, lectus sed imperdiet aliquam, augue ipsum rhoncus augue, ac rhoncus quam risus sit amet justo. Nunc bibendum viverra facilisis. Etiam lacinia volutpat diam, vitae aliquet nisi maximus et. Fusce non massa ac nisl dapibus lacinia.
</p>
<p>
Nullam maximus vulputate fringilla. Quisque quis odio interdum, egestas massa vel, facilisis ante. Aliquam et nulla sodales ligula rhoncus imperdiet. Praesent mollis purus vitae dictum egestas. Aliquam ante massa, aliquam sed diam non, pulvinar tristique nunc. Pellentesque quis ex eget leo lobortis pharetra. Aliquam vitae eleifend urna, a varius ante. Nam euismod urna ut iaculis ultricies. Donec id purus feugiat, tristique nisl id, sollicitudin nisi.
</p>
<p>
Morbi purus tellus, efficitur pulvinar mi a, porta egestas sem. Nullam vitae urna sapien. Vestibulum eu arcu turpis. Morbi laoreet aliquam sagittis. Praesent mattis leo velit, ac porttitor tellus venenatis eu. Aliquam vitae egestas ex. Duis quis efficitur sem, ut venenatis ipsum. Nunc iaculis massa vel ligula eleifend, id pulvinar odio accumsan.
</p>
<p>
Pellentesque vehicula magna eu massa placerat elementum. Aliquam quis nisi non nisl tincidunt ullamcorper a non eros. Cras volutpat vehicula libero, nec bibendum libero ornare eleifend. Ut fermentum, massa sed condimentum varius, elit turpis gravida dui, et efficitur ex risus et eros. Morbi venenatis orci at metus malesuada ornare. Fusce vulputate dignissim purus, quis placerat tellus ornare in. Nulla facilisi. Morbi eu nisl eget erat consectetur finibus vel in odio. Nullam dictum gravida odio, eget ultricies metus pulvinar nec. Mauris vitae tellus enim. Aenean eu lorem sollicitudin, congue orci eu, aliquet nisl. Etiam lacinia felis eget augue pellentesque pharetra. Morbi gravida nibh quis commodo consectetur.
</p>
<p>
Nulla facilisi. Donec sodales arcu quis nibh vulputate tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec egestas facilisis fringilla. Sed lacus justo, pulvinar vitae purus nec, venenatis congue ligula. Fusce auctor ac ex sit amet tempus. In hac habitasse platea dictumst. Cras feugiat et quam ut interdum. Sed dapibus erat id odio porttitor tempus. Nam malesuada risus et sapien varius vehicula. Vivamus faucibus sapien at erat rutrum suscipit. Maecenas ullamcorper tincidunt augue, vel fermentum purus sagittis non. Quisque a felis in quam vulputate auctor eget et nunc. Curabitur ut nisl vulputate magna ultrices aliquet. Proin blandit et lorem non dapibus.
</p>`)
]);

export const snippets = [simple,/* hardCase,*/ allTags, plainText, viewPageCase];
