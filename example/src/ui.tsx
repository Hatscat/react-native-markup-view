import * as React from 'react';
import { View, Text, TouchableOpacity, ScrollView, Modal } from 'react-native';

export enum Font {
  Oswald = 'Oswald',
  Lato = 'Lato',
  Times = 'Times',
}

export function ButtonGroup({ children, ...props }: React.ComponentProps<typeof View> & { children: React.ReactNode }) {
  return (
    <View {...props}>
      <ScrollView
        horizontal={true}
        contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      >
        {children}
      </ScrollView>
    </View>
  );
}

export function Button(props: React.ComponentProps<typeof TouchableOpacity> & { title: string; active?: boolean }) {
  return (
    <TouchableOpacity
      {...props}
      style={[
        Button.defaultStyle,
        props.disabled ? { opacity: 0.5 } : {},
        { borderColor: props.active ? 'orange' : 'rgba(0,0,0,0.5)' },
        props.style,
      ]}
    >
      <Text>{props.title}</Text>
    </TouchableOpacity>
  );
}
Button.defaultStyle = {
  height: 40,
  paddingHorizontal: 8,
  margin: 2,
  backgroundColor: '#AEE',
  borderRadius: 8,
  borderWidth: 1,
  alignItems: 'center',
  justifyContent: 'center',
} as const;

export function ToggleButton({
  onValueChange,
  onPress,
  value,
  title,
  ...otherProps
}: React.ComponentProps<typeof Button> & {
  value: boolean;
  onValueChange?: (value: boolean) => void;
}) {
  const checkbox = value ? '☑' : '☐';
  return (
    <Button
      {...otherProps}
      title={checkbox + ' ' + title}
      onPress={(event) => {
        if (onPress) {
          onPress(event);
        }
        if (onValueChange) {
          onValueChange(!value);
        }
      }}
    />
  );
}

export function Select<V>({
  onValueChange,
  onPress,
  value,
  choices,
  ...otherProps
}: Omit<React.ComponentProps<typeof Button>, 'title'> & {
  choices: Array<{ label: string; value: V }>;
  value: V;
  onValueChange?: (value: V) => void;
}) {
  const [open, setOpen] = React.useState(false);
  const close = () => setOpen(false);

  return (
    <>
      <Button
        {...otherProps}
        title={choices.find((choice) => choice.value === value)?.label + ' ▼'}
        onPress={(event) => {
          if (onPress) {
            onPress(event);
          }
          setOpen(true);
        }}
      />
      <Modal visible={open} onRequestClose={close}>
        <View style={{ flexDirection: 'column', justifyContent: 'center', flex: 1, padding: 16 }}>
          <View>
            {choices.map((choice, choiceIndex) => (
              <Button
                key={'choice-' + choiceIndex}
                title={choice.label}
                onPress={() => {
                  if (onValueChange) {
                    onValueChange(choice.value);
                  }
                  close();
                }}
                style={{ marginTop: 16 }}
                active={choice.value === value}
              />
            ))}
            <Button title={'Cancel'} onPress={close} style={{ marginTop: 32 }} />
          </View>
        </View>
      </Modal>
    </>
  );
}
