import { StyleSheet, View, Text, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';
import * as React from 'react';
import MarkupView, {
  ContentTextSource,
  Highlight,
  SelectionChangeEvent,
  Selection,
  HighlightPressEvent,
} from '@koober/react-native-markup-view';
import { CollapsibleView } from './CollapsibleView';
import { Font } from './ui';
import localFonts from './fonts';

export type MarkupPageProps = {
  counter: number;
  fontFamily: string;
  fontSize: number;
  highlightColor: undefined | string;
  highlights: Highlight[];
  parentCollapsible: boolean;
  scrollEnabled: boolean;
  selectable: boolean;
  selection: undefined | Selection;
  setSelection: (selection: undefined | Selection) => void;
  setCurrentHighlight: (highlight: undefined | Highlight) => void;
  source: ContentTextSource;
};

export default function MarkupPage({
  selection,
  setSelection,
  source,
  scrollEnabled,
  parentCollapsible,
  selectable,
  counter,
  fontFamily,
  fontSize,
  highlights,
  highlightColor,
  setCurrentHighlight,
}: MarkupPageProps) {
  const [collapsed, setCollapsed] = React.useState(true);
  const [ready, setReady] = React.useState(false);

  const handleReady = () => {
    setReady(true);
  };
  const handleSelection = (event: SelectionChangeEvent) => {
    console.debug('onSelectionChange', event.selection);
    setSelection(event.selection);
  };
  const handleHighlightPress = (event: HighlightPressEvent) => {
    setCurrentHighlight(event.highlight);
  };

  const renderText = (fontFamily: string, fontSize: number) => (
    <Text style={{ fontSize, fontFamily }}>
      {fontFamily} {fontSize}px
    </Text>
  );
  const renderListItem = (label: string, value: string) => (
    <View style={{ flexDirection: 'row' }}>
      <View style={{ minWidth: 70 }}>
        <Text style={{ fontSize: 10 }}>{label}</Text>
      </View>
      <View>
        <Text style={{ fontSize: 10 }}>{value}</Text>
      </View>
    </View>
  );

  const renderTextHTML = (fontFamily: string, fontSize: number) =>
    `<span style="font-family:${fontFamily};font-size:${fontSize}px">${fontFamily} ${fontSize}px<br/></span>`;

  const renderFrame = (title: string, content: React.ReactNode) => (
    <>
      <View style={{ backgroundColor: 'rgba(0,0,0,0.3)', paddingHorizontal: 4 }}>
        <Text style={{ fontSize: 8, color: 'rgba(0,0,0,0.7)' }}>{title}</Text>
      </View>
      {content}
    </>
  );

  const renderContainer = parentCollapsible
    ? (fragment: React.ReactNode) => (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              width: '100%',
              height: 30,
              flexDirection: 'row',
              alignContent: 'center',
              justifyContent: 'center',
            }}
            onPress={() => setCollapsed(!collapsed)}
          >
            <Text style={{ paddingVertical: 4 }}>
              {collapsed ? '▲' : '▼'} Toggle collapse {collapsed ? '▲' : '▼'}
            </Text>
          </TouchableOpacity>
          <ScrollView style={{ flex: 1 }}>
            <CollapsibleView collapsedMin={15} collapsed={collapsed}>
              {fragment}
            </CollapsibleView>
          </ScrollView>
        </View>
      )
    : !scrollEnabled
    ? (fragment: React.ReactNode) => <ScrollView style={styles.container}>{fragment}</ScrollView>
    : (fragment: React.ReactNode) => <View style={styles.container}>{fragment}</View>;

  return renderContainer(
    <>
      {renderFrame(
        '<Text />',
        <View>
          {renderListItem('Ready', String(ready))}
          {renderListItem('Selection', selection ? `start: ${selection.start} end:${selection.end}` : 'none')}

          {renderText(fontFamily, 24)}
          {renderText(fontFamily, MarkupView.defaultFontSize)}
        </View>
      )}
      {renderFrame(
        '<MarkupView />',
        <MarkupView
          source={{
            encoding: source.encoding,
            content: `${renderTextHTML(fontFamily, 24)}${renderTextHTML(fontFamily, MarkupView.defaultFontSize)}
    <h3>Clicked ${counter} times!</h3>
    ${source.content}
    `,
          }}
          styleSheet={{
            html: {
              fontSize: fontSize + 'px',
            },
            body: {
              fontFamily,
            },
          }}
          fonts={[
            /* Local only */
            {
              fontFamily: `'${Font.Oswald}'`,
              src: localFonts[Font.Oswald],
            },
            {
              fontFamily: `'${Font.Lato}'`,
              fontStyle: 'normal',
              fontWeight: '400',
              src: `local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v16/S6uyw4BMUTPHjx4wXiWtFCc.woff2) format('woff2')`,
              unicodeRange:
                'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD',
            },
          ]}
          selectable={selectable}
          highlightColor={highlightColor}
          onReady={handleReady}
          onSelectionChange={handleSelection}
          onHighlightPress={handleHighlightPress}
          scrollEnabled={parentCollapsible ? false : scrollEnabled}
          style={{ flexGrow: 1, borderColor: 'red', borderWidth: 1 }}
          highlights={highlights}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  markupView: {
    flexGrow: 10,
  },
});
